// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'package:test/test.dart';

import 'package:fast_chemail/src/ascii.dart' as ascii;
import 'package:fast_chemail/fastchemail.dart';

void main() {
  final checkChar = ascii.CheckChar();
  final checkInt = ascii.CheckInt();

  group("isLetter", () {
    test("", () {
      expect(checkChar.isLetter('a'), equals(true));
      expect(checkChar.isLetter('j'), equals(true));
      expect(checkChar.isLetter('z'), equals(true));
      expect(checkChar.isLetter('A'), equals(true));
      expect(checkChar.isLetter('J'), equals(true));
      expect(checkChar.isLetter('Z'), equals(true));
      expect(checkChar.isLetter('0'), equals(false));

      expect(checkInt.isLetter('a'.codeUnitAt(0)), equals(true));
      expect(checkInt.isLetter('0'.codeUnitAt(0)), equals(false));
    });
  });

  group("isDigit", () {
    test("", () {
      expect(checkChar.isDigit('0'), equals(true));
      expect(checkChar.isDigit('5'), equals(true));
      expect(checkChar.isDigit('9'), equals(true));
      expect(checkChar.isDigit('a'), equals(false));

      expect(checkInt.isDigit('0'.codeUnitAt(0)), equals(true));
      expect(checkInt.isDigit('a'.codeUnitAt(0)), equals(false));
    });
  });

  group("checkASCIIPrintable", () {
    test("", () {
      expect(ascii.checkASCIIPrintable("foo"), equals(null));

      String inputErr = "foó";
      expect(() => ascii.checkASCIIPrintable(inputErr),
          throwsA(TypeMatcher<NonAsciiException>()));
      try {
        ascii.checkASCIIPrintable(inputErr);
      } catch (e) {
        expect(e.charCode, "ó".codeUnitAt(0));
      }

      inputErr = "foo\tbar";
      expect(() => ascii.checkASCIIPrintable(inputErr),
          throwsA(TypeMatcher<ControlCharException>()));
      try {
        ascii.checkASCIIPrintable(inputErr);
      } catch (e) {
        expect(e.position, 4);
      }
    });
  });
}

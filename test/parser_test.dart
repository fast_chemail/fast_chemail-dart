// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'package:test/test.dart';

import 'package:fast_chemail/src/parser.dart' as parser;
import 'package:fast_chemail/fastchemail.dart';

final TABLE_OK = [
  r"!#$%&'*+-/=?^_`{|}~@example.com",
  "user+mailbox@example.com",
  "customer/department=shipping@example.com",
  r"$A12345@example.com",
  "!def!xyz%abc@example.com",
  "_somename@example.com",
  "a@example.com",
  "a@x.y",
  "abc.def@example.com",
  "abc-def@example.com",
  "123@example.com",
  "xn--abc@example.com",
  "abc@x.y.z",
  "abc@xyz-example.com",
  "abc@c--n.com",
  "abc@xn--hxajbheg2az3al.xn--jxalpdlp",
  "abc@1example.com",
  "abc@x.123",
];

class ErrorTest {
  final String input;
  final TypeMatcher excep;
  String char = "";
  int position = 0;

  ErrorTest(this.input, this.excep);
  ErrorTest.char(this.input, this.excep, this.char);
  ErrorTest.pos(this.input, this.excep, this.position);
}

final List<ErrorTest> TABLE_ERR = [
  ErrorTest("@", TypeMatcher<NoLocalPartException>()),
  ErrorTest("@example.com", TypeMatcher<NoLocalPartException>()),
  ErrorTest("abc@", TypeMatcher<NoDomainPartException>()),
  ErrorTest("abc", TypeMatcher<NoSignAtException>()),
  ErrorTest("abc@def@example.com", TypeMatcher<TooAtException>()),
  ErrorTest(".abc@example.com", TypeMatcher<LocalStartPeriodException>()),
  ErrorTest("abc.@example.com", TypeMatcher<LocalEndPeriodException>()),
  ErrorTest("abc@.example.com", TypeMatcher<DomainStartPeriodException>()),
  ErrorTest("abc@example.com.", TypeMatcher<DomainEndPeriodException>()),
  ErrorTest("ab..cd@example.com", TypeMatcher<ConsecutivePeriodException>()),
  ErrorTest("abc@example..com", TypeMatcher<ConsecutivePeriodException>()),
  ErrorTest("a@example", TypeMatcher<NoPeriodDomainException>()),
  ErrorTest.char(r"ab\c@example.com", TypeMatcher<WrongCharLocalException>(), '\\'),
  ErrorTest.char(r'ab"c"def@example.com', TypeMatcher<WrongCharLocalException>(), '"'),
  ErrorTest.char("abc def@example.com", TypeMatcher<WrongCharLocalException>(), ' '),
  ErrorTest.char("(comment)abc@example.com", TypeMatcher<WrongCharLocalException>(), '('),
  ErrorTest.char("abc@[255.255.255.255]", TypeMatcher<WrongCharDomainException>(), '['),
  ErrorTest.char("abc@(example.com", TypeMatcher<WrongCharDomainException>(), '('),
  ErrorTest.char("abc@x.y_y.z", TypeMatcher<WrongCharDomainException>(), '_'),
  ErrorTest.char("abc@-example.com", TypeMatcher<WrongStartLabelException>(), '-'),
  ErrorTest.char("abc@example-.com", TypeMatcher<WrongEndLabelException>(), '-'),
  ErrorTest.char("abc@x.-y.z", TypeMatcher<WrongStartLabelException>(), '-'),
  ErrorTest.char("abc@x.y-.z", TypeMatcher<WrongEndLabelException>(), '-'),
  ErrorTest.char("abcd€f@example.com", TypeMatcher<NonAsciiException>(), '€'),
  ErrorTest.char("abc@exámple.com", TypeMatcher<NonAsciiException>(), 'á'),
  ErrorTest.pos("a\tbc@example.com", TypeMatcher<ControlCharException>(), 2),
  ErrorTest.pos("abc@\texample.com", TypeMatcher<ControlCharException>(), 5),
];

void main() {
  group("parseEmail", () {
    test("", () {
      for (var x in TABLE_OK) {
        expect(parseEmail(x), equals((null)));
      }
    });

    test("", () {
      for (var x in TABLE_ERR) {
        var except = x.excep;
        //expect(() => parseEmail(x.input), throwsA(TypeMatcher<except>()));
        expect(() => parseEmail(x.input), throwsA(except));

        if (x.char != "" || x.position != 0) {
          try {
            parseEmail(x.input);
          } on ControlCharException catch (e) {
            expect(e.position, x.position);
          } catch (e) {
            expect(e.charCode, x.char.codeUnitAt(0));
          }
        }
      }
    });
  });

  group("isValidEmail", () {
    test("", () => expect(isValidEmail(TABLE_OK[0]), true));
    test("", () => expect(isValidEmail(TABLE_ERR[0].input), false));
  });

  String localPart = "";
  for (var i = 0; i < parser.MAX_LOCAL_PART; i++) {
    localPart = "${localPart}a";
  }

  String label = "";
  for (var i = 0; i < parser.MAX_LABEL; i++) {
    label = "${label}x";
  }
  label = "${label}.";

  String allLabels = "";
  for (var i = 0; i < 3; i++) {
    allLabels = "${allLabels}${label}";
  }

  String lastLabel = "";
  for (var i = 0; i < parser.MAX_DOMAIN_PART - allLabels.length; i++) {
    lastLabel = "${lastLabel}y";
  }

  group("length", () {
    test("", () {
      expect(
          parseEmail("${localPart}@${allLabels}${lastLabel}"), equals((null)));
    });

    test("", () {
      expect(() => parseEmail("a${localPart}@${allLabels}${lastLabel}"),
          throwsA(TypeMatcher<LocalTooLongException>()));
    });
    test("", () {
      expect(() => parseEmail("${localPart}@${allLabels}${lastLabel}z"),
          throwsA(TypeMatcher<DomainTooLongException>()));
    });
    test("", () {
      expect(() => parseEmail("${localPart}@${label}x${lastLabel}"),
          throwsA(TypeMatcher<LabelTooLongException>()));
    });
  });
}

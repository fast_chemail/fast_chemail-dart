// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'package:fast_chemail/src/ascii.dart' as ascii;
import 'package:fast_chemail/src/parser.dart' as parser;

final _check = ascii.CheckInt();

/// `isValidEmail` checks wheter an email address is valid.
bool isValidEmail(String address) {
  try {
    parseEmail(address);
  } catch (_) {
    return false;
  }
  return true;
}

/// `parseEmail` scans an email address to check wheter it is correct.
parseEmail(String address) {
  if (address.startsWith('@')) {
    throw NoLocalPartException();
  }
  if (address.endsWith('@')) {
    throw NoDomainPartException();
  }

  // https://tools.ietf.org/html/rfc5321#section-4.1.2
  //
  // Systems MUST NOT define mailboxes in such a way as to require the use in
  // SMTP of non-ASCII characters (octets with the high order bit set to one)
  // or ASCII "control characters" (decimal value 0-31 and 127).
  try {
    ascii.checkASCIIPrintable(address);
  } catch (e) {
    throw e;
  }

  final parts = address.split('@');
  if (parts.length != 2) {
    if (parts.length > 2) {
      throw TooAtException();
    }
    throw NoSignAtException();
  }

  // == Local part

  // https://tools.ietf.org/html/rfc3696#section-3
  //
  // Period (".") may also appear, but may not be used to start or end
  // the local part, nor may two or more consecutive periods appear.

  if (parts[0].length > parser.MAX_LOCAL_PART) {
    throw LocalTooLongException();
  }
  if (parts[0].startsWith('.')) {
    throw LocalStartPeriodException();
  }
  if (parts[0].endsWith('.')) {
    throw LocalEndPeriodException();
  }

  bool lastPeriod = false;
  for (var char in parts[0].runes) {
    if (_check.isLetter(char) || _check.isDigit(char)) {
      if (lastPeriod) {
        lastPeriod = false;
      }
      continue;
    }

    if (char == ascii.PERIOD_SIGN) {
      if (lastPeriod) {
        throw ConsecutivePeriodException();
      }
      lastPeriod = true;
      continue;
    }

    if (char == ascii.EXCLAMATION_MARK_SIGN || // '!'
            char == ascii.NUMBER_SIGN || // '#'
            char == ascii.DOLLAR_SIGN || // r'$'
            char == ascii.PERCENT_SIGN || // '%'
            char == ascii.AMPERSAND_SIGN || // '&'
            char == ascii.APOSTROPHE_SIGN || // '\''
            char == ascii.ASTERISK_SIGN || // '*'
            char == ascii.PLUS_SIGN || // '+'
            char == ascii.MINUS_SIGN || // '-'
            char == ascii.SLASH_SIGN || // '/'
            char == ascii.EQUALS_SIGN || // '='
            char == ascii.QUESTION_MARK_SIGN || // '?'
            char == ascii.CARET_SIGN || // '^'
            char == ascii.UNDERSCORE_SIGN || // '_'
            char == ascii.GRAVE_ACCENT_SIGN || // '`'
            char == ascii.LEFT_CURLY_BRACKET_SIGN || // '{'
            char == ascii.VERTICAL_BAR_SIGN || // '|'
            char == ascii.RIGHT_CURLY_BRACKET_SIGN || // '}'
            char == ascii.TILDE_SIGN // '~'
        ) {
      if (lastPeriod) {
        lastPeriod = false;
      }
      continue;
    }

    throw WrongCharLocalException(char);
  }

  // == Domain part

  // https://tools.ietf.org/html/rfc5321#section-4.1.2
  //
  // characters outside the set of alphabetic characters, digits, and hyphen
  // MUST NOT appear in domain name labels for SMTP clients or servers.  In
  // particular, the underscore character is not permitted.

  // https://tools.ietf.org/html/rfc1034#section-3.5
  //
  // The labels must follow the rules for ARPANET host names.  They must start
  // with a letter, end with a letter or digit, and have as interior
  // characters only letters, digits, and hyphen.  There are also some
  // restrictions on the length.  Labels must be 63 characters or less.

  // https://tools.ietf.org/html/rfc3696#section-2
  //
  // It is likely that the better strategy has now become to make the "at
  // least one period" test, to verify LDH conformance (including verification
  // that the apparent TLD name is not all-numeric), and then to use the DNS
  // to determine domain name validity, rather than trying to maintain a local
  // list of valid TLD names.

  if (parts[1].length > parser.MAX_DOMAIN_PART) {
    throw DomainTooLongException();
  }
  if (parts[1].startsWith('.')) {
    throw DomainStartPeriodException();
  }
  if (parts[1].endsWith('.')) {
    throw DomainEndPeriodException();
  }

  final labels = parts[1].split('.');
  if (labels.length == 1) {
    throw NoPeriodDomainException();
  }

  // label = let-dig [ [ ldh-str ] let-dig ]
	// limited to a length of 63 characters by RFC 1034 section 3.5
	//
	// <let-dig> ::= <letter> | <digit>
	// <ldh-str> ::= <let-dig-hyp> | <let-dig-hyp> <ldh-str>
	// <let-dig-hyp> ::= <let-dig> | "-"
  for (var label in labels) {
    if (label.length == 0) {
      throw ConsecutivePeriodException();
    }
    if (label.length > parser.MAX_LABEL) {
      throw LabelTooLongException();
    }

    for (var char in label.runes) {
      if (_check.isLetter(char) ||
          _check.isDigit(char) ||
          char == ascii.MINUS_SIGN) {
        continue;
      }
      throw WrongCharDomainException(char);
    }

    final firstChar = label.codeUnitAt(0);
    if (!_check.isLetter(firstChar) && !_check.isDigit(firstChar)) {
      throw WrongStartLabelException(label.codeUnitAt(0));
    }
    final lastChar = label.codeUnitAt(label.length - 1);
    if (!_check.isLetter(lastChar) && !_check.isDigit(lastChar)) {
      throw WrongEndLabelException(lastChar);
    }
  }
}

// == Errors
//

class NoLocalPartException implements Exception {
  String toString() => "no local part";
}

class NoDomainPartException implements Exception {
  String toString() => "no domain part";
}

class NoSignAtException implements Exception {
  String toString() => "no at sign (@)";
}

class TooAtException implements Exception {
  String toString() => "wrong number of at sign (@)";
}

class LocalTooLongException implements Exception {
  String toString() =>
      "the local part has more than ${parser.MAX_LOCAL_PART} characters";
}

class DomainTooLongException implements Exception {
  String toString() =>
      "the domain part has more than ${parser.MAX_DOMAIN_PART} characters";
}

class LabelTooLongException implements Exception {
  String toString() =>
      "a domain label has more than ${parser.MAX_LABEL} characters";
}

class LocalStartPeriodException implements Exception {
  String toString() => "the local part starts with a period";
}

class LocalEndPeriodException implements Exception {
  String toString() => "the local part ends with a period";
}

class DomainStartPeriodException implements Exception {
  String toString() => "the domain part starts with a period";
}

class DomainEndPeriodException implements Exception {
  String toString() => "the domain part ends with a period";
}

class ConsecutivePeriodException implements Exception {
  String toString() => "appear two or more consecutive periods";
}

class NoPeriodDomainException implements Exception {
  String toString() => "no period at domain part";
}

class WrongCharLocalException implements Exception {
  final int charCode;
  const WrongCharLocalException(this.charCode);

  String toString() {
    final char = String.fromCharCode(this.charCode);
    return "character not valid in local part ($char)";
  }
}

class WrongCharDomainException implements Exception {
  final int charCode;
  const WrongCharDomainException(this.charCode);

  String toString() {
    final char = String.fromCharCode(this.charCode);
    return "character not valid in domain part ($char)";
  }
}

class WrongStartLabelException implements Exception {
  final int charCode;
  const WrongStartLabelException(this.charCode);

  String toString() {
    final char = String.fromCharCode(this.charCode);
    return "character not valid at start of domain label ($char)";
  }
}

class WrongEndLabelException implements Exception {
  final int charCode;
  const WrongEndLabelException(this.charCode);

  String toString() {
    final char = String.fromCharCode(this.charCode);
    return "character not valid at end of domain label ($char)";
  }
}

// == ASCII

class NonAsciiException implements Exception {
  final int charCode;
  const NonAsciiException(this.charCode);

  String toString() {
    final char = String.fromCharCode(this.charCode);
    return "contain non US-ASCII character ($char)";
  }
}

class ControlCharException implements Exception {
  final int position;
  const ControlCharException(this.position);

  String toString() {
    return "contain ASCII control character at position $this.position";
  }
}

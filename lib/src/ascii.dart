// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'package:fast_chemail/fastchemail.dart';

const MAX_ASCII = 127;

final A_LOWER = 'a'.codeUnitAt(0);
final Z_LOWER = 'z'.codeUnitAt(0);
final A_UPPER = 'A'.codeUnitAt(0);
final Z_UPPER = 'Z'.codeUnitAt(0);
final DIG_0 = '0'.codeUnitAt(0);
final DIG_9 = '9'.codeUnitAt(0);

abstract class Checker<T> {
  bool isLetter(T value);
  bool isDigit(T value);
}

class CheckChar implements Checker<String> {
  bool isLetter(String char) {
    final code = char.codeUnitAt(0);

    if ((code >= A_LOWER && code <= Z_LOWER) ||
        (code >= A_UPPER && code <= Z_UPPER)) {
      return true;
    }
    return false;
  }

  bool isDigit(String char) {
    final code = char.codeUnitAt(0);
    return DIG_0 <= code && code <= DIG_9;
  }
}

class CheckInt implements Checker<int> {
  bool isLetter(int code) {
    if ((code >= A_LOWER && code <= Z_LOWER) ||
        (code >= A_UPPER && code <= Z_UPPER)) {
      return true;
    }
    return false;
  }

  bool isDigit(int code) {
    return DIG_0 <= code && code <= DIG_9;
  }
}

// `checkASCIIPrintable` reports an error wheter the string has a non-ASCII
// character or any ASCII control character.
checkASCIIPrintable(String str) {
  int pos = 1;
  for (var code in str.codeUnits) {
    if (code > MAX_ASCII) {
      throw NonAsciiException(code);
    }
    if (0 <= code && code <= 31 || code == 127) {
      throw ControlCharException(pos);
    }
    pos++;
  }
}

// * * *

final EXCLAMATION_MARK_SIGN = '!'.codeUnitAt(0);
final NUMBER_SIGN = '#'.codeUnitAt(0);
final DOLLAR_SIGN = r'$'.codeUnitAt(0);
final PERCENT_SIGN = '%'.codeUnitAt(0);
final AMPERSAND_SIGN = '&'.codeUnitAt(0);
final APOSTROPHE_SIGN = '\''.codeUnitAt(0);
final ASTERISK_SIGN = '*'.codeUnitAt(0);
final PLUS_SIGN = '+'.codeUnitAt(0);
final MINUS_SIGN = '-'.codeUnitAt(0);
final SLASH_SIGN = '/'.codeUnitAt(0);
final EQUALS_SIGN = '='.codeUnitAt(0);
final QUESTION_MARK_SIGN = '?'.codeUnitAt(0);
final CARET_SIGN = '^'.codeUnitAt(0);
final UNDERSCORE_SIGN = '_'.codeUnitAt(0);
final GRAVE_ACCENT_SIGN = '`'.codeUnitAt(0);
final LEFT_CURLY_BRACKET_SIGN = '{'.codeUnitAt(0);
final VERTICAL_BAR_SIGN = '|'.codeUnitAt(0);
final RIGHT_CURLY_BRACKET_SIGN = '}'.codeUnitAt(0);
final TILDE_SIGN = '~'.codeUnitAt(0);

final PERIOD_SIGN = '.'.codeUnitAt(0);
